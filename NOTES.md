# Notes

- [Onu UI](https://onu.zyob.top/)
- https://carrd.co/build#profile
- https://daisyui.com/docs/install/
- https://github.com/Code-Pop/real-world-vue
- https://eslint.vuejs.org/user-guide/#visual-studio-code
- `vue create profile-card-1 --no-git` ([Vue CLI](https://cli.vuejs.org/))
- https://github.com/zloirock/core-js (polyfills)
- https://github.com/octref/veturpack/blob/master/.prettierrc.json
- https://code.visualstudio.com/docs/languages/jsconfig + https://vuejs.github.io/vetur/guide/setup.html#project-setup
- https://prettier.io/docs/en/options.html#end-of-line
- https://cli.vuejs.org/guide/deployment.html#gitlab-pages + https://hub.docker.com/_/node
- https://cli.vuejs.org/guide/cli-service.html#vue-cli-service-serve
- https://www.w3.org/WAI/tutorials/images/decorative/
- `npm install --save-dev prettier eslint-plugin-prettier eslint-config-prettier`
- [Vue Devtools](https://devtools.vuejs.org/)

Minimal `jsconfig.json`:

```json
{
  "compilerOptions": {
    "baseUrl": ".",
    "paths": {
      "@/*": ["./src/*"]
    }
  }
}
```

## [Real World Vue 3 (Composition API)](https://www.vuemastery.com/courses/real-world-vue-3-composition-api/building-a-vue-3-app-composition-api) course by Adam Jahr/Vue Mastery

- [create-vue](https://github.com/vuejs/create-vue)
- [Repo](https://github.com/Code-Pop/Real-World-Vue-3-New-Syntax)
- `<EventCard v-for="event in events" :key="event.id" :event="event" />`
- [Vue Router](https://router.vuejs.org/). Vue Router helps in implementing client-side routing (vs. server-side/"traditional" routing). It renders differences without reloading the page.
- Single-page application (SPA): a web app that loads from a single page (`index.html`) and dynamically updates that page as the user interacts with it.
- [Axios](https://axios-http.com/)
- [My JSON Server](https://my-json-server.typicode.com/). [Repo](https://github.com/typicode/json-server). [Demo](https://github.com/typicode/demo).
- In [history mode](https://router.vuejs.org/guide/essentials/history-mode.html), we are taking advantage of the browser's `history.pushState()` API to change the URL without reloading the page. [Caveat](https://router.vuejs.org/guide/essentials/history-mode.html#caveat).
- Since the app is a SPA, everything needs to be served from the `index.html` file. We need to configure Render's [Redirects and Rewrites](https://render.com/docs/redirects-rewrites) rules so that it always knows to serve up `index.html`, no matter what URL we navigate to:
  - Source: `/*`
  - Destination: `/index.html`
  - Action: `Rewrite`

---

```css
#layout {
  font-family: Avenir, Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
```

---

```js
const events = ref(null)

onMounted(() => {
  axios
    .get('https://my-json-server.typicode.com/Code-Pop/Real-World_Vue-3/events')
    .then((response) => {
      events.value = response.data
    })
    .catch((error) => {
      console.log(error)
    })
})
```

vs.

```js
// src/services/EventService.js file
import axios from 'axios'

const apiClient = axios.create({
  baseURL: 'https://my-json-server.typicode.com/Code-Pop/Real-World_Vue-3',
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

export default {
  getEvents() {
    return apiClient.get('/events')
  }
}
```

and

```js
import EventService from '@/services/EventService.js'

const events = ref(null)

onMounted(() => {
  EventService.getEvents()
    .then((response) => {
      events.value = response.data
    })
    .catch((error) => {
      console.log(error)
    })
})
```

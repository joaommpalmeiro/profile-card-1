import Vue from 'vue'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'

import App from './App.vue'

// https://v2.vuejs.org/v2/api/#productionTip
Vue.config.productionTip = false

// https://buefy.org/documentation/constructor-options
// https://fonts.google.com/icons
Vue.use(Buefy, { defaultIconPack: 'mdi' })

new Vue({ render: (h) => h(App) }).$mount('#app')

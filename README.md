# profile-card-1

## Screenshots

![Full page](assets/Screenshot%202023-01-19%20at%2018.00.15.png)

### Hover the photo

![Hover the photo](assets/Screenshot%202023-01-19%20at%2018.00.21.png)

### Click on the dropdown menu

![Click on the dropdown menu](assets/Screenshot%202023-01-19%20at%2018.00.29.png)

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

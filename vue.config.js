const { defineConfig } = require('@vue/cli-service')

module.exports = defineConfig({
  // https://cli.vuejs.org/config/#transpiledependencies
  transpileDependencies: true,
  // https://cli.vuejs.org/core-plugins/eslint.html#configuration
  lintOnSave: false,
  // https://cli.vuejs.org/config/#devserver
  // https://webpack.js.org/configuration/dev-server/#devserveropen
  // https://github.com/vuejs/vue-cli/issues/2051
  // https://github.com/vuejs/vue-cli/issues/4421#issuecomment-966723688
  // https://webpack.js.org/configuration/watch/#watchoptionspoll
  devServer: {
    open: false
  },
  // For WSL:
  configureWebpack: {
    watchOptions: {
      poll: true
    }
  },
  // https://cli.vuejs.org/config/#publicpath
  // https://cli.vuejs.org/guide/deployment.html#gitlab-pages
  publicPath:
    process.env.NODE_ENV === 'production'
      ? '/' + process.env.CI_PROJECT_NAME + '/'
      : '/'
})
